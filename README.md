# Architecture of project services 
- Nginx
- Tomcat
- RabbitMQ
- Memcached 
- Mysql
# Architecture for automated setup
- Vagrant
- Virtualbox
- Gitbash
# To bring up VMs
- cd /vagrant
- cd /automated_provisioning
- vagrant up

